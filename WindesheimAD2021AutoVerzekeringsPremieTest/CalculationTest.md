﻿# Testen Eindopdracht

In dit markdown bestand zal ik toelichten welke keuzes ik heb gemaakt en waarom. 
Ik zal verder toelichten voor welke data en technieken ik heb gekozen.

### Policyholder

Voor de policyholder mock heb ik random waarde bedacht. 
Daardoor heb ik gekozen voor 35 jaar oud, 15 jaar rijbewijs, postcode 7922 en 0 geclaimde jaren. De reden van deze gegevens is dat ik hierdoor geen rekening hoef te houden met de premie opslag van 15% . Hierdoor zullen de sommen makkelijker zijn te controleren.

### Vehicle

Voor de vehicle mock heb ik een random auto aangelikt op Autoscout24.nl.
De auto kost 10000 euro, heeft een vermogen van 142kW en is gebouwd in 2010.

#### PremiumCalculationTest

Hier wordt de basis premie berekent. In de hoofdcode stond er in de berekening een kleiner dan of gelijk aan 5, ` <=5 `, hier hoorde echter kleiner dan 5, ` <5 `, te staan. 
Na het aanpassen van de hoofdcode, wordt er in ons geval een resultaat van 39 verwacht, dit was ook het geval. 
De test is gelukt en werkt.

#### PayMonthlyNoDiscountTest

Hier testen we of het bedrag bij maandelijkse betaling goed is. 
Hier gebruik ik een Fact voor aangezien hier geen verschillende gevallen voor zijn.
Er is maar 1 goed antwoord en dat is het juiste antwoord.
De test is goed gelukt

#### PayYearlyTotalAddDiscountTest

Hier testen we net als bij de maandelijkse betaling test of het bedrag bij jaarlijkse betaling goed is en er rekening wordt gehouden met de 2,5% korting.
Hier gebruik ik een Fact voor aangezien hier geen verschillende gevallen voor zijn.
Er is maar 1 goed antwoord en dat is het juiste antwoord.
De test is goed gelukt

#### PostalCheckBetweenPreDefinedRangesForAddedCostTest

Hier testen we of op bepaalde postcodes extra moet worden betaald, dit is op basis van eerder gegeven postcode bereiken.
Hierbij heb ik gebruik gemaakt van Theory met verschillende waardes en een DoubleExpectation toegepast.
Deze testen zijn goed verlopen en hadden het gewenste resultaat.

#### CheckEveryCoverageTypeResultTest

Hier testen we de 3 types van verzekeringen op de uitkomst die zij zouden moeten hebben zoals beschreven in de opdracht.
Hierbij heb ik gebruik gemaakt van Fact in plaats van Theory. 
Hier is voor gekozen omdat we hiervoor niet verschillende gevallen of grenswaardes hoeven te testen. 
Alle 3 de berekeningen zijn rounded zodat het eindgetal mooi wordt afgerond op 2 getalen na de komma.

#### HolderPolicyAge23OrLicenseYoungerThen5YearsAdd15PercentTest

Hier testen we of de policyholder extra moet betalen wegens zijn leeftijd als hij jonger is dan 23 of zijn rijbewijs minder dan 5 jaar heeft. 
Ook hier maken we gebruik van Theory met InlineData zodat wij verschillende gevallen en randwaardes kunnen testen.
De testen verliepen goed en gaven het gewenste resultaat.

#### CheckNoClaimDiscount6YearsOrHigherTest

Hier testen we of de policyholder recht heeft op een korting bij 6 of meer jaren schade vrij. 
Om dit te testen heb ik gebruik gemaakt van InlineData en Theory om alle mogelijke gevallen te testen. 
Hierbij heb ik voornamelijk de grenswaarden getest. Deze testen zijn ook goed verlopen.
