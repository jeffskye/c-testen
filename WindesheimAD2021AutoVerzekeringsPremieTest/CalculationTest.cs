﻿using System;
using Xunit;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class CalculationTest
    {
        private static readonly double BasePremium = 39;

        private static Mock<PolicyHolder> PolicyHolderMocked (int age = 35, int licenseAge = 15, int postalCode = 7922, int noClaimYears = 0)
        {
            var policyHolderMocked = new Mock<PolicyHolder>( MockBehavior.Default, 35, "24-02-2006", 7922, 0 );
            policyHolderMocked.Setup(policyholder => policyholder.NoClaimYears).Returns(noClaimYears);
            policyHolderMocked.Setup(policyholder => policyholder.LicenseAge).Returns(licenseAge);
            policyHolderMocked.Setup(policyholder => policyholder.Age).Returns(age);
            policyHolderMocked.Setup(policyholder => policyholder.PostalCode).Returns(postalCode);
            return policyHolderMocked;
        }

        private static Mock<Vehicle> VehicleMocker (int powerInKW = 142, int valueInEuros = 10000, int age = 11)
        {
            var vehicleMocked = new Mock<Vehicle>( 142, 10000, 2010 );
            vehicleMocked.Setup( vehicle => vehicle.PowerInKw ).Returns( powerInKW );
            vehicleMocked.Setup( vehicle => vehicle.ValueInEuros ).Returns( valueInEuros );
            vehicleMocked.Setup( vehicle => vehicle.Age ).Returns( age );
            return vehicleMocked;
        }

        [Fact]
        public void PremiumCalculationTest()
        {
            // Arrange
            double expectResult = 39;
            Vehicle car = new Vehicle(142, 10000, 2010);

            // Act
            double basePremium = Math.Round(PremiumCalculation.CalculateBasePremium(car), 2);

            // Assert
            Assert.Equal(expectResult, basePremium);
        }

        [Fact]
        public void PayMonthlyNoDiscountTest()
        {
            // Arrange
            double expectMonthlyOut = 3.25;
            var policyHolderMocked = PolicyHolderMocked().Object;
            var vehicleMocked = VehicleMocker().Object;

            // Act
            var actualMonthlyOut = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            // Assert
            Assert.Equal(expectMonthlyOut, actualMonthlyOut);
        }

        [Fact]
        public void PayYearlyTotalAddDiscountTest()
        {
            // Arrange
            double expectOutYearly = 39;
            var policyHolderMocked = PolicyHolderMocked().Object;
            var vehicleMocked = VehicleMocker().Object;

            // Act
            var actualOutYearly = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            // Assert
            Assert.Equal(expectOutYearly, Math.Round(actualOutYearly * 1.025));
        }

        [Fact]
        public void CheckEveryCoverageTypeResultTest()
        {
            // Arrange
            var policyHolderMocked = PolicyHolderMocked().Object;
            var vehicleMocked = VehicleMocker().Object;
            double WA_expect = 39;
            double WA_PLUSexpect = 46.8;
            double ALL_RISKexpect = 78;

            // Act
            var outALL_RISK = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.ALL_RISK);
            var roundALL_RISK = Math.Round(outALL_RISK.PremiumAmountPerYear, 2);
            var outWA_PLUS = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA_PLUS);
            var roundWA_PLUS = Math.Round(outWA_PLUS.PremiumAmountPerYear, 2);
            var outWA = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA);
            var roundWA = Math.Round(outWA.PremiumAmountPerYear, 2);

            // Assert
            Assert.Equal(ALL_RISKexpect, roundALL_RISK);
            Assert.Equal(WA_PLUSexpect, roundWA_PLUS);
            Assert.Equal(WA_expect, roundWA);
        }

        [Theory]
        [InlineData( 4500 , 1.00 )]
        [InlineData( 3600 , 1.02 )]
        [InlineData( 4499 , 1.02 )]
        [InlineData( 1334 , 1.05 )]
        [InlineData( 3599 , 1.05 )]
        [InlineData( 1000 , 1.05 )]

        public void PostalCheckBetweenPreDefinedRangesForAddedCostTest(int postalCode, double expectation)
        {
            // Arrange
            var policyHolderMocked = PolicyHolderMocked(postalCode: postalCode).Object;
            var vehicleMocked = VehicleMocker().Object;

            // Act
            var expectMulti = Math.Round(expectation * BasePremium, 2);
            var actualOut = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA);
            var actuaOutDouble = Math.Round(actualOut.PremiumAmountPerYear, 2);

            // Assert
            Assert.Equal(expectMulti, actuaOutDouble);
        }

        [Theory]
        [InlineData( 25 , 5  , 1.00 )]
        [InlineData( 30 , 11 , 1.00 )]
        [InlineData( 30 , 2  , 1.15 )]
        [InlineData( 20 , 5  , 1.15 )]

        public void HolderPolicyAge23OrLicenseYoungerThen5YearsAdd15PercentTest(int age, int licenseAge, double expectation)
        {
            // Arrange
            var vehicleMocked = VehicleMocker().Object;
            var policyHolderMocked = PolicyHolderMocked(age, licenseAge).Object;

            // Act
            var actualOut = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA);
            var actuaOutDouble = Math.Round(actualOut.PremiumAmountPerYear, 2);
            var expectMulti = Math.Round(expectation * BasePremium, 2);

            // Assert
            Assert.Equal(expectMulti, actuaOutDouble);
        }

        [Theory]
        [InlineData( 2  , 1.00 )]
        [InlineData( 5  , 1.00 )]
        [InlineData( 6  , 0.95 )]
        [InlineData( 12 , 0.65 )]
        [InlineData( 17 , 0.40 )]
        [InlineData( 18 , 0.35 )]
        [InlineData( 19 , 0.35 )]
        [InlineData( 25 , 0.35 )]

        public void CheckNoClaimDiscount6YearsOrHigherTest(int noClaimYears, double expectation)
        {
            // Arrange
            var policyHolderMocked = PolicyHolderMocked(noClaimYears: noClaimYears).Object;
            var vehicleMocked = VehicleMocker().Object;

            // Act
            var actualOut = new PremiumCalculation(vehicleMocked, policyHolderMocked, InsuranceCoverage.WA);
            var actualOutDouble = Math.Round(actualOut.PremiumAmountPerYear, 2);
            var expectMulti = Math.Round((expectation * BasePremium), 2);

            // Assert
            Assert.Equal(expectMulti, actualOutDouble);
        }

    }
}
